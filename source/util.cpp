#include "util.hpp"

std::string util::getDeviceNick() {
    setsysInitialize();
    char nick[0x80] = {0};
    setsysGetDeviceNickname(nick);
    setsysExit();
    return std::string(nick);
}

std::string util::getSha256(const std::string& data) {
    char hash[SHA256_HASH_SIZE];
    sha256CalculateHash(&hash, data.c_str(), data.size());

    return util::makeHex((const char *)&hash, SHA256_HASH_SIZE);
}

std::string util::getConsoleSerialHash() {
    setsysInitialize();
    char serial[0x80] = {0};
    setsysGetSerialNumber(serial);
    setsysExit();
    return util::getSha256(std::string(serial));
}

std::string util::getFirmwareVersion() {
    setsysInitialize();
    SetSysFirmwareVersion fwVersion;
    setsysGetFirmwareVersion(&fwVersion);
    setsysExit();
    return std::string(fwVersion.display_version);
}

/**
 * Do a full string replace in-place.
 * @param data
 * @param toSearch
 * @param replaceStr
 */
void util::replace(std::string &data, std::string toSearch, std::string replaceStr) {
    // Get the first occurrence
    size_t pos = data.find(toSearch);

    // Repeat till end is reached
    while (pos != std::string::npos) {
        // Replace this occurrence of Sub String
        data.replace(pos, toSearch.size(), replaceStr);
        // Get the next occurrence from the current position
        pos = data.find(toSearch, pos + replaceStr.size());
    }
}

/**
 * Make bytes hex
 * @param key raw data
 * @param length Amount of bytes
 * @return String of double the length
 */
std::string util::makeHex(const char* key, int length) {
    char const hex_chars[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    std::string string;
    for(auto i = 0; i < length; ++i )
    {
        char const byte = key[i];

        string += hex_chars[ ( byte & 0xF0 ) >> 4 ];
        string += hex_chars[ ( byte & 0x0F ) >> 0 ];
    }
    return string;
}

/**
 * Returns a hex string of the given length (must be divisible by 2)
 * @param length
 * @return
 */
std::string util::randomString(u32 length) {
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_entropy_context entropy;
    unsigned char key[1024];

    int ret;


    mbedtls_entropy_init(&entropy);
    mbedtls_ctr_drbg_init(&ctr_drbg);

    if ((ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func, &entropy, nullptr, 0)) != 0) {
        printf(" failed\n ! mbedtls_ctr_drbg_init returned -0x%04x\n", -ret);
        return "";
    }

    if ((ret = mbedtls_ctr_drbg_random(&ctr_drbg, key, length/2)) != 0) {
        printf(" failed\n ! mbedtls_ctr_drbg_random returned -0x%04x\n", -ret);
        return "";
    }

    return util::makeHex((const char *)&key, length / 2);
}


void util::mkdirp(std::string dir) {
    char tmp[256];
    char *p = NULL;
    size_t len;

    snprintf(tmp, sizeof(tmp), "%s", dir.c_str());
    len = strlen(tmp);
    if (tmp[len - 1] == '/')
        tmp[len - 1] = 0;
    for (p = tmp + 1; *p; p++)
        if (*p == '/') {
            *p = 0;
            mkdir(tmp, S_IRWXU);
            *p = '/';
        }
    mkdir(tmp, S_IRWXU);
}

bool util::deleteFolder(std::string folder) {
    DIR *dir;
    struct dirent *entry;
    char path[PATH_MAX];

    dir = opendir(folder.c_str());
    if (dir == NULL) {
        return false;
    }

    while ((entry = readdir(dir)) != NULL) {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, "..")) {
            snprintf(path, (size_t) PATH_MAX, "%s/%s", folder.c_str(), entry->d_name);
            if (entry->d_type == DT_DIR) {
                util::deleteFolder(path);
                continue;
            }

            printf("Deleting: %s\n", path);
            unlink(path);
        }

    }
    closedir(dir);

    /*
     * Now the directory is emtpy, finally delete the directory itself. (Just
     * printing here, see above)
     */
    printf("Deleting: %s\n", folder.c_str());
    remove(folder.c_str());

    return true;

}