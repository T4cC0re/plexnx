#include "Player.hpp"

#define FFMPEG_COLOR_PALETTE AV_PIX_FMT_RGBA

bool ffinit = false;
bool stop = false;
bool isPaused = false;
AVFormatContext* ctx_format;
AVCodecContext* ctx_codec;
AVCodec* codec;
AVFrame* frame;
AVFrame* resizedFrame;
AVRational* rational;
int stream_idx;
SwsContext* ctx_sws;
AVStream *vid_stream;
AVPacket* pkt;
int ret;
int iffw = 1;
int counter = 0;
string videoPath;
double fps;
FramePacer* fp;
u32 skip = 0;

void Player::playbackInit(string VideoPath)
{
    if(!ffinit)
    {
        printf("initializing playback of '%s' via " LIBAVFORMAT_IDENT "\n", VideoPath.c_str());
        videoPath = VideoPath;
#ifdef DEBUG
        av_log_set_level(AV_LOG_DEBUG);
#elif defined VERBOSE
        av_log_set_level(AV_LOG_VERBOSE);
#else
        av_log_set_level(AV_LOG_QUIET);
#endif
        frame = av_frame_alloc();
        resizedFrame = av_frame_alloc();
        pkt = av_packet_alloc();

        avformat_network_init();

        ret = avformat_open_input(&ctx_format, VideoPath.c_str(), NULL, NULL);
        if(ret != 0) {
            averr(0, -ret);
            Player::playbackThrowError("Error opening file. Is it a valid video?");
        }
        if(avformat_find_stream_info(ctx_format, NULL) < 0) Player::playbackThrowError("Error finding stream info.");
        av_dump_format(ctx_format, 0, VideoPath.c_str(), false);
        for(u32 i = 0; i < ctx_format->nb_streams; i++) if(ctx_format->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            stream_idx = i;
            vid_stream = ctx_format->streams[i];
            break;
        }
        if(!vid_stream) Player::playbackThrowError("Error getting video stream.");
        codec = avcodec_find_decoder(vid_stream->codecpar->codec_id);
        if(!codec) Player::playbackThrowError("Error finding a decoder (strange)");
        ctx_codec = avcodec_alloc_context3(codec);
        if(avcodec_parameters_to_context(ctx_codec, vid_stream->codecpar) < 0) Player::playbackThrowError("Error sending parameters to codec context.");


        // Get video stream info
        rational = &ctx_codec->time_base;
        printf( "numerator is %i\n", rational->num);
        printf( "denominator is %i\n", rational->den);
        printf( "duration is %ld\n", ctx_format->duration);
        fps = vid_stream->avg_frame_rate.num / (double)vid_stream->avg_frame_rate.den;
        printf( "fps is %lf\n", fps);
        printf( "wanted frametime is %lf µs\n", 1000000 / fps);

        fp = new FramePacer(1000000 / fps);

        // Enable automatic multithreading in codec
        AVDictionary* info = NULL;
        av_dict_set(&info, "threads", "auto", 0);
        if(avcodec_open2(ctx_codec, codec, &info) < 0) Player::playbackThrowError("Error opening codec with context.");
        ffinit = true;
        printf("playback initialized\n");
        appletSetMediaPlaybackState(true);
    }
}

bool Player::playbackLoop()
{
    ctx_sws = nullptr;
    u64 frameCount = 0;
    resizedFrame->width = Gfx::FrameWidth;
    resizedFrame->height = Gfx::FrameHeight;
    resizedFrame->format = FFMPEG_COLOR_PALETTE;
//    printf("before alloc\n");
    av_image_alloc(resizedFrame->data, resizedFrame->linesize, Gfx::FrameWidth, Gfx::FrameHeight, FFMPEG_COLOR_PALETTE, 32);
//    printf("after alloc\n");
    free(resizedFrame->data[0]); // We'll set this to Gfx::currentFrame later.
//    printf("after 1st framebuffer hack\n");

    if(ffinit)
    {
        while(appletMainLoop())
        {
//            printf("while\n");
            if(stop) break;
            if(isPaused)
            {
                hidScanInput();
                u64 k = hidKeysDown(CONTROLLER_P1_AUTO);
                if(k & KEY_X) Player::playbackPauseResume();
                else if((k & KEY_PLUS) || (k & KEY_MINUS)) Player::playbackStop();
                else if(k & KEY_Y)
                {
                    int newffd = 1;
                    if(iffw == 1) newffd = 2;
                    else if(iffw == 2) newffd = 4;
                    else if(iffw == 4) newffd = 8;
                    else newffd = 1;
                    Player::playbackSetFastForward(newffd);
                }
            }
            else if(av_read_frame(ctx_format, pkt) >= 0)
            {
//                printf("av_read_frame\n");
                if(pkt->stream_index == stream_idx)
                {
//                    printf("pkt->stream_index == stream_idx\n");
                    ret = avcodec_send_packet(ctx_codec, pkt);
                    if(ret < 0 || ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
//                            printf("avcodec_send_packet err\n");
                            break;
                        }
                    }

//                    printf("avcodec_send_packet ok\n");
                    while(appletMainLoop() && ret >= 0)
                    {
//                        printf("while 2\n");
                        ret = avcodec_receive_frame(ctx_codec, frame);
                        if(ret < 0 || ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                            if (ret == AVERROR(EAGAIN)) {
//                                printf("avcodec_receive_frame 2\n");
                                break;
                            }
                            averr(2, ret);
                        }
                        if (frame == nullptr) {
                            haltf("frame == nullptr")
                        }
//                        printf("before input\n");
                        hidScanInput();
                        u64 k = hidKeysDown(CONTROLLER_P1_AUTO);
                        if(k & KEY_X) Player::playbackPauseResume();
                        else if((k & KEY_PLUS) || (k & KEY_MINUS)) Player::playbackStop();
                        else if(k & KEY_Y)
                        {
                            int newffd = 1;
                            if(iffw == 1) newffd = 2;
                            else if(iffw == 2) newffd = 4;
                            else if(iffw == 4) newffd = 8;
                            else if(iffw == 8) newffd = 1;
                            Player::playbackSetFastForward(newffd);
                        }
//                        printf("after input\n");
                        counter++;
                        // Fast forward handling
                        if(counter >= iffw) counter = 0;
                        else continue;

//                        printf("before pace\n");
                        if((skip = fp->pace()) > PACE_OK) {
                            printf("frame delivered too late! Need to skip %d frame(s)\n", skip);
                            continue;
                        }
//                        printf("after pace\n");

                        //Scale input
                        ctx_sws = sws_getCachedContext(ctx_sws, frame->width, frame->height, (AVPixelFormat)frame->format, resizedFrame->width, resizedFrame->height, FFMPEG_COLOR_PALETTE, SWS_BILINEAR, 0, 0, 0);

//                        printf("after sws_getCachedContext\n");
                        Gfx::startFrame();

//                        printf("after startFrame\n");
                        // Hackery to get sws_scale to directly write to the framebuffer, which saves us a memcpy
//                        {
//                            std::unique_lock<std::mutex> lock(Gfx::mutex);
//                            printf("after lock\n");
                            resizedFrame->data[0] = (uint8_t*)Gfx::currentFrame;
//                            printf("after uint8_t\n");
                            sws_scale(ctx_sws, frame->data, frame->linesize, 0, frame->height, resizedFrame->data, resizedFrame->linesize);

//                            printf("after sws_scale\n");
                            gwrite(0, 0, 20, "Frame: %lu", frameCount);
                            gwrite(0, 25, 20, "File: %s", videoPath.c_str());
                            gwrite(0, 50, 20, "FramePacer FPS: %f", fp->getFPS());
                            gwrite(0, 75, 20, "desired FPS: %lf", fps);

//                            printf("after gwrite\n");
//                        }
                        Gfx::endFrame();
//                        printf("after endFrame\n");
                        frameCount++;
                    }
                }
            }
            else stop = true;
        }
        resizedFrame->data[0] = nullptr; // Because we have previously set this to be the Gfx::currentFrame directly, we need to set this to a nullptr, otherwise we free our Gfx::currentFrame
        av_freep(resizedFrame->data);
        if(stop) Player::playbackExit();
        return false;
    }
    return false;
}

void Player::playbackPause()
{
    if(!isPaused) isPaused = true;
}

void Player::playbackResume()
{
    if(isPaused) isPaused = false;
}

void Player::playbackPauseResume()
{
    isPaused = !isPaused;
}

void Player::playbackStop()
{
    if(!stop) stop = true;
}

void Player::playbackExit()
{
    if(ffinit)
    {
        appletSetMediaPlaybackState(false);
        avformat_close_input(&ctx_format);
        av_packet_unref(pkt);
        av_frame_unref(resizedFrame);
        av_frame_unref(frame);
        avcodec_free_context(&ctx_codec);
        avformat_free_context(ctx_format);
        avformat_network_deinit();
        delete fp;
        if(stop) stop = false;
        if(isPaused) isPaused = false;
        iffw = 1;
        ffinit = false;
    }
}

void Player::playbackSetFastForward(int Power)
{
    iffw = Power;
}

void Player::playbackThrowError(string Error)
{
    haltf(APP_TITLE_STR "was closed due to an error: \n\n\n - %s\n\n\n - Press + or - to exit " APP_TITLE_STR, Error.c_str());
}

vector<string> Player::getVideoFiles()
{
    vector<string> vids;
    struct dirent *de;
    DIR *dir = opendir("sdmc:/media");
    if(dir)
    {
        while(true)
        {
            de = readdir(dir);
            if(de == NULL) break;
            string name = "/media/";
            name.append(de->d_name);
            vids.push_back(name);
        }
    }
    closedir(dir);
    return vids;
}