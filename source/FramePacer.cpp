#include "Include.hpp"

FramePacer::FramePacer(const int frameInterval) {
    this->frameInterval = frameInterval;
    this->frames = -1;

    printf("Initialized FramePacer @ %d µs\n", this->frameInterval);
    this->fpsReference = std::chrono::high_resolution_clock::now();
    this->nextFrame = std::chrono::high_resolution_clock::now() + std::chrono::microseconds(this->frameInterval);
};

FramePacer::~FramePacer() {
}

int FramePacer::pace() {
    this->frames++;

    //First frame is not subject to timing.
    if (this->frames == 0) {
        this->nextFrame = std::chrono::high_resolution_clock::now() + std::chrono::microseconds(this->frameInterval);
        return PACE_OK;
    }

    auto now = std::chrono::high_resolution_clock::now();
    if (now > this->nextFrame) {
        double diff = std::chrono::duration_cast<std::chrono::microseconds>(now - this->nextFrame).count() + this->frameInterval;
        this->nextFrame = now + std::chrono::microseconds(this->frameInterval);
        int r = std::lround(diff / this->frameInterval);

        //Don't time next frame then.
        this->frames = -1;
        return r;
    }

    // libNX way to sleep
    svcSleepThread(std::chrono::duration_cast<std::chrono::nanoseconds>(
            this->nextFrame - now).count());

    // New now, because we slept.
    this->nextFrame = std::chrono::high_resolution_clock::now() + std::chrono::microseconds(this->frameInterval);
    return PACE_OK;
}

double FramePacer::getFPS() {
    double us = std::chrono::duration_cast<std::chrono::microseconds>(
            std::chrono::high_resolution_clock::now() - this->fpsReference).count();

    double fps = this->frames / us * 1000000;

    // Reset every second
    if (this->frames % ((int) 1000000 / this->frameInterval) == 0) {
        this->fpsReference = std::chrono::high_resolution_clock::now();
        this->frames = 0;
    }

    return fps;
}