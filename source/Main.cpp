#include "Include.hpp"
#include "Player.hpp"

#ifdef GPU_PALETTE_CONVERSION
#include "GPU.hpp"
#endif


void renderBackgroundAndAppName() {
    DRAW_BG();
    Gfx::drawText(170, 43, STRINGIFY(APP_TITLE), {255,255,255,255}, 24);
    Gfx::drawText(170, 78, STRINGIFY(APP_VERSION), {255,255,255,255}, 14);
}
//
//// region error handling
//alignas(16) u8 __nx_exception_stack[0x1000];
//u64 __nx_exception_stack_size = sizeof(__nx_exception_stack);
//
//void __libnx_exception_handler(ThreadExceptionDump *ctx)
//{
////    int i;
////    FILE *f = fopen("exception_dump", "w");
////    if(f==NULL)return;
////
////    fprintf(f, "error_desc: 0x%x\n", ctx->error_desc);//You can also parse this with ThreadExceptionDesc.
////    //This assumes AArch64, however you can also use threadExceptionIsAArch64().
////    for(i=0; i<29; i++)fprintf(f, "[X%d]: 0x%lx\n", i, ctx->cpu_gprs[i].x);
////    fprintf(f, "fp: 0x%lx\n", ctx->fp.x);
////    fprintf(f, "lr: 0x%lx\n", ctx->lr.x);
////    fprintf(f, "sp: 0x%lx\n", ctx->sp.x);
////    fprintf(f, "pc: 0x%lx\n", ctx->pc.x);
////
////    //You could print fpu_gprs if you want.
////
////    fprintf(f, "pstate: 0x%x\n", ctx->pstate);
////    fprintf(f, "afsr0: 0x%x\n", ctx->afsr0);
////    fprintf(f, "afsr1: 0x%x\n", ctx->afsr1);
////    fprintf(f, "esr: 0x%x\n", ctx->esr);
////
////    fprintf(f, "far: 0x%lx\n", ctx->far.x);
////
////    fclose(f);
//    if (!Gfx::currentFrame) {
//        Gfx::startFrame();
//    }
//    int i;
//    gwrite(0, 0,  "error_desc: 0x%x\n", ctx->error_desc);//You can also parse this with ThreadExceptionDesc.
//    //This assumes AArch64, however you can also use threadExceptionIsAArch64().
//    for(i=0; i<29; i++) {
//        gwrite(0, (i+1)*25, "[X%d]: 0x%lx\n", i, ctx->cpu_gprs[i].x);
//    }
//    i++;
//    gwrite(0, i*25, "fp: 0x%lx\n", ctx->fp.x); i++;
//    gwrite(0, i*25,"lr: 0x%lx\n", ctx->lr.x); i++;
//    gwrite(0, i*25, "sp: 0x%lx\n", ctx->sp.x); i++;
//    gwrite(0, i*25, "pc: 0x%lx\n", ctx->pc.x); i++;
//
//    //You could print fpu_gprs if you want.
//
//    gwrite(0, i*25, "pstate: 0x%x\n", ctx->pstate); i++;
//    gwrite(0, i*25, "afsr0: 0x%x\n", ctx->afsr0); i++;
//    gwrite(0, i*25, "afsr1: 0x%x\n", ctx->afsr1); i++;
//    gwrite(0, i*25, "esr: 0x%x\n", ctx->esr); i++;
//
//    gwrite(0, i*25, "far: 0x%lx\n", ctx->far.x);
//
//    Gfx::endFrame();
//
//}
//// endregion

int64_t last = 0;

int progress_callback(void *p,   int64_t dltotal,   int64_t dlnow,   int64_t ultotal,   int64_t ulnow){
    if (dlnow - last > 1024*100) {
    gprintf("UP: %" CURL_FORMAT_CURL_OFF_T "M of %" CURL_FORMAT_CURL_OFF_T
    "M DOWN: %" CURL_FORMAT_CURL_OFF_T "M of %" CURL_FORMAT_CURL_OFF_T
    "M\n",
            ulnow / 1024 / 1024, ultotal / 1024 / 1024, dlnow / 1024 / 1024, dltotal / 1024 / 1024);
        last = dlnow;
    }

    return 0;
}

/*
int demo(){
    CURLcode res;
    CURL *curl;
    FILE *fp;

    char outfilename[FILENAME_MAX] = "sdmc:/media/test.mkv"; // file used for curl_easy_setopt
    char url[1024] = "https://plex.t4cc0.re:32400/library/parts/910/1564276438/file.mkv?download=1&X-Plex-Token=yVXDGVezcxQnz8HgHrPU";

    curl = curl_easy_init();
    if (curl) {
        last =0;
        gprintf("\n\nURL = %s\nFile = %s\n\nDownload...\n", url, outfilename);
        fp = fopen(outfilename, "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_USERAGENT, TARGET "/" APP_VERSION);

        curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 10240000L);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");

        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long) CURL_HTTP_VERSION_2TLS); //Try HTTP2

        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
        curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 1L);

        struct curl_slist *headers = NULL;

        string nick = util::getDeviceNick();
        string serialHash = util::getConsoleSerialHash();
        string firmware = util::getFirmwareVersion();
        headers = curl_slist_append(headers, "X-Plex-Platform: NX");
        headers = curl_slist_append(headers, (std::string("X-Plex-Platform-Version: ") + firmware).c_str());
        headers = curl_slist_append(headers, (std::string("X-Plex-Client-Identifier: ") + serialHash).c_str());
        headers = curl_slist_append(headers, "X-Plex-Product: " APP_TITLE);
        headers = curl_slist_append(headers, "X-Plex-Version: " APP_VERSION);
        headers = curl_slist_append(headers, "X-Plex-Device: NintendoSwitch");
        headers = curl_slist_append(headers, (std::string("X-Plex-Device-Name: ") + nick).c_str());
//        headers = curl_slist_append(headers, "Range: bytes=0-10240000");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);


#ifdef VERBOSE
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); // debugging
#else
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L); // debugging
#endif

        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_callback);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L); // following HTTP redirects
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 3600L); // a max of timeout setopt for 20s
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER,
                         0L); // skipping cert. verification, if needed (ACTUALLY IS NEEDED FROM LIBNX)
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST,
                         0L); // skipping hostname verification, if needed (ACTUALLY IS NEEDED FROM LIBNX)
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback); // writes data into pointer
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp); // writes pointer into FILE *destination
        res = curl_easy_perform(curl); // perform tasks curl_easy_setopt asked before
        if (res != CURLE_OK) {
            gprintf("\nfailed: %s\n", curl_easy_strerror(res));
        } else {
            gprintf("done!");
        }
        curl_easy_cleanup(curl); // always cleanup
        curl_slist_free_all(headers); // Free header list before checking curl result
        fclose(fp); // closing FILE *fp
    } else {
        gprintf("curl initAuto failed");
    }
}
*/

void showVideoMenu(const PlexItem& item) {
    gprintf("Launching movie/video %s", item.title.c_str());
    auto url = Plex::getInstance()->getPlaybackUrl(item);

    if (appletMainLoop()){
        Player::playbackInit(url);
    }
    Gfx::clear({255, 0, 0, 255});
    while(appletMainLoop() && Player::playbackLoop());
}

void showKey(const string& key, const PlexServer& server) {
    load:
    Gfx::startFrame();
    renderBackgroundAndAppName();
    Gfx::drawText(70, 170, "Please wait...", {255,255,255,255}, 20);
    Gfx::endFrame();
    const std::vector<PlexItem>& items = Plex::getInstance()->openKey(key,server);
    List <PlexItem> itemList;
    itemList.noItemsText = "Either you are offline, or there is no content here.\nPress X to refresh";
    itemList.offsetX = 50;
    itemList.offsetY = 150;

    for (unsigned n = 0; n < items.size(); ++n) {
        itemList.addItem(items.at(n));
    }

    render:
    Gfx::startFrame();
    renderBackgroundAndAppName();
    Gfx::drawText(750, 10, "Press A to enter the selected item", {192,192,192,255}, 18);
    Gfx::drawText(750, 30, "Press Up and Down to move the selection", {192,192,192,255}, 18);
    Gfx::drawText(750, 50, "Press B to return to the section overview", {192,192,192,255}, 18);
    Gfx::drawText(750, 70, "Press X to refresh the item list", {192,192,192,255}, 18);
    itemList.render();
    Gfx::endFrame();

    while (appletMainLoop()) {
        u64 k = 0;
        while(appletMainLoop() && !k) {
            hidScanInput();
            k = hidKeysDown(CONTROLLER_P1_AUTO);
        }
        if ((k & KEY_A)) {
            const PlexItem& plexItem = itemList.items.at(itemList.getSelectedIndex());

            if (plexItem.type.find("movie") != std::string::npos) {
                // A single entity in "movie"or "other videos" always shows up as movie
                // This is why we filter interactions on this to start playback/info page
                showVideoMenu(plexItem);
            } else  if (plexItem.type.find("episode") != std::string::npos) {
                // An episode will show up separately from "show" or "season" when in
                // a TV-Shows section. We do the same as above, but for an episode in a show.
                // is episode invocation
                // TODO: implement
                gprintf("TODO Launching episode %s", plexItem.title.c_str());
                while (true) {
                    ifAnyButton {
                            break;
                    };
                }
            } else {
                // If it is of literally any other type, just load the key and display it again.
                // This allows for minimal logic to realize movie, show and season display with the
                // same logic.
                showKey(plexItem.key, server);
            }
            goto render;
        } else if ((k & KEY_B)) {
            return;
        } else if ((k & KEY_UP)) {
            itemList.selectUp();
            goto render;
        } else if ((k & KEY_DOWN)) {
            itemList.selectDown();
            goto render;
        } else if ((k & KEY_X)) {
            goto load;
        }
    }
}

void serverMenu(const PlexServer& ps) {
    load:
    Gfx::startFrame();
    renderBackgroundAndAppName();
    Gfx::drawText(70, 170, "Please wait...", {255,255,255,255}, 20);
    Gfx::endFrame();
    const std::vector<PlexSection>& sections = Plex::getInstance()->connectToServer(ps);
    List <PlexSection> sectionList;
    sectionList.noItemsText = "Either you are offline, or the server does not have any sections.\nPress X to refresh";
    sectionList.offsetX = 50;
    sectionList.offsetY = 150;

    for (unsigned n = 0; n < sections.size(); ++n) {
        sectionList.addItem(sections.at(n));
    }

    render:
    Gfx::startFrame();
    renderBackgroundAndAppName();
    Gfx::drawText(750, 10, "Press A to browse the selected section", {192,192,192,255}, 18);
    Gfx::drawText(750, 30, "Press Up and Down to move the selection", {192,192,192,255}, 18);
    Gfx::drawText(750, 50, "Press B to return to the Plex server overview", {192,192,192,255}, 18);
    Gfx::drawText(750, 70, "Press X to refresh the section list", {192,192,192,255}, 18);
    sectionList.render();
    Gfx::endFrame();

    while (appletMainLoop()) {
        u64 k = 0;
        while(appletMainLoop() && !k) {
            hidScanInput();
            k = hidKeysDown(CONTROLLER_P1_AUTO);
        }
        if ((k & KEY_A)) {
            const PlexSection& plexSection = sectionList.items.at(sectionList.getSelectedIndex());

            std::string key = std::string("/library/sections/").append(std::to_string(plexSection.id)).append("/all");
            showKey(key, plexSection.server);
            goto render;
        } else if ((k & KEY_B)) {
            return;
        } else if ((k & KEY_UP)) {
            sectionList.selectUp();
            goto render;
        } else if ((k & KEY_DOWN)) {
            sectionList.selectDown();
            goto render;
        } else if ((k & KEY_X)) {
            goto load;
        }
    }
}


int main() {
    romfsInit();
    Gfx::init();
    Result ret = socketInitializeDefault();
//    haltf("after net initAuto");
    if (ret != 0) {
        haltf("NET INIT FAILED: %d", ret);
    }

    nxlinkStdio();
    printf(APP_TITLE_STR
    " "
    APP_VERSION_STR
    "\n");
    printf("libavformat version: " LIBAVFORMAT_IDENT "\n");
    printf("libCURL version: %s\n", curl_version());
    printf("Device name: %s\n", util::getDeviceNick().c_str());
    printf("Device FW: %s\n", util::getFirmwareVersion().c_str());
    printf("Device ID (serial hash): %s\n", util::getConsoleSerialHash().c_str());

    Clock::runOC(1683);
    appletSetWirelessPriorityMode(AppletWirelessPriorityMode_Unknown2);

    // Give NSP loading animation some time to finish.
//    svcSleepThread(1000 * 1000 * 1000);
    plex:

    Gfx::startFrame();
    renderBackgroundAndAppName();
    Gfx::drawText(70, 170, "Please wait... Loading Plex servers..\nKick back, relax and wait a few seconds :)", {255,255,255,255}, 20);
    Gfx::endFrame();
    const std::vector<struct PlexServer>& servers = Plex::getInstance()->getServers();

    List <PlexServer> serverList;
    serverList.noItemsText = "Either you, or all your servers are offline (or your token has expired)\nPress Y to re-link your device\nPress X to retry connecting";
    serverList.offsetX = 50;
    serverList.offsetY = 150;

    for (unsigned n = 0; n < servers.size(); ++n) {
        serverList.addItem(servers.at(n));
    }

    render:
    Gfx::startFrame();
    renderBackgroundAndAppName();
    Gfx::drawText(750, 10, "Press A to connect to the selected server", {192,192,192,255}, 18);
    Gfx::drawText(750, 30, "Press Up and Down to move the selection", {192,192,192,255}, 18);
    Gfx::drawText(750, 50, "Press Y to (re-)link your Plex account", {192,192,192,255}, 18);
    Gfx::drawText(750, 70, "Press X to rescan your Plex servers", {192,192,192,255}, 18);
    Gfx::drawText(750, 90, "Press + to quit", {192,192,192,255}, 18);
    Gfx::drawText(750, 110, "Press L to clear thumbnail caches", {192,192,192,255}, 18);
    serverList.render();
    Gfx::endFrame();

    while (appletMainLoop()) {
        u64 k = 0;
        while(appletMainLoop() && !k) {
            hidScanInput();
            k = hidKeysDown(CONTROLLER_P1_AUTO);
        }
        if (k & KEY_A) {
            PlexServer ps = serverList.items.at(serverList.getSelectedIndex());
            serverMenu(ps);
            goto render;
        } else if ((k & KEY_Y)) {
            Plex::getInstance()->authRoutine();
            goto plex;
        } else if ((k & KEY_X)) {
            goto plex;
        } else if ((k & KEY_PLUS)) {
            cleanup();
            exit(0);
        } else if ((k & KEY_UP)) {
            serverList.selectUp();
            goto render;
        } else if ((k & KEY_DOWN)) {
            serverList.selectDown();
            goto render;
        } else if ((k & KEY_L)) {
            util::deleteFolder("/cache/" TARGET_STR);
            goto render;
        }
    }

    return 0;
}
/*
    // Old stuff below
    Gfx::drawText(50, 150,
                  "Local videos:",
                  {255, 255, 255, 255}, 20);
    u32 bx = 50;
    u32 by = 170;
    if(vids.empty()) {
        printf("no videos found\n");
        Gfx::drawText(bx, by,
                      "No video files were found.\nPlace them at 'sdmc:/media' folder.\nSupported formats: MP4, MKV, 3GP, AVI, FLV, WMV, WEBM",
                      {255, 255, 255, 255}, 20);
    }
    for (unsigned n=0; n<vids.size(); ++n) {
        printf("found video '%s'\n", vids.at(n).c_str());
    }
    for(u32 i = 0; i < vids.size(); i++)
    {
        Gfx::RGBA color = { 255, 255, 255, 255 };
        if(i == selected) color = { 180, 180, 255, 255 };
        if(i > 20)
        {
            bx += 500;
            if(i == 20) by = 120;
        }
        Gfx::drawText(bx, by, vids[i], color, 18);
        by += 25;
    }
    Gfx::drawText(bx, by,
                  "Plex servers:",
                  {255, 255, 255, 255}, 20);
    by += 25;
    for(u32 i = 0; i < servers.size(); i++)
    {
        Gfx::RGBA color = { 255, 255, 255, 255 };
        if(i == selected) color = { 180, 180, 255, 255 };
        if(i > 20)
        {
            bx += 500;
            if(i == 20) by = 120;
        }
        Gfx::drawText(bx, by, servers.at(i).name.c_str(), color, 18);
        by += 25;
    }
    Gfx::endFrame();
    while(appletMainLoop())
    {
        hidScanInput();
        u64 k = hidKeysDown(CONTROLLER_P1_AUTO);
        if((k & KEY_A) & (!vids.empty())) break;
        else if((k & KEY_Y))
        {
            Plex::getInstance()->authRoutine();
            goto plex;
        }
        else if((k & KEY_PLUS) || (k & KEY_MINUS))
        {
            cleanup();
            exit(0);
        }
        else if(k & KEY_X)
        {
            Gfx::startFrame();
            Gfx::drawImage(0, 0, "romfs:/background.jpg");
            Gfx::drawText(80, 120, "Controls (for video playback):\n\nPress X to pause/resume video playback.\nPress Y to fast-forward (switching values between 1x, 2x, 4x and 8x)\nPress Plus or Minus to exit video playback.\n\nNow, press A or B to go back to video selection.", { 255, 255, 255, 255 }, 20);
            Gfx::endFrame();
            while(appletMainLoop())
            {
                hidScanInput();
                u64 k2 = hidKeysDown(CONTROLLER_P1_AUTO);
                if((k2 & KEY_A) || (k2 & KEY_B))
                {
                    goto start;
                }
                else if((k2 & KEY_PLUS) || (k2 & KEY_MINUS))
                {
                    Gfx::exit();
                    romfsExit();
                    return 0;
                }
            }
        }
        else if(k & KEY_UP)
        {
            Gfx::startFrame();
            if(selected > 0)
            {
                selected--;
                goto start;
            }
            Gfx::endFrame();
        }
        else if(k & KEY_DOWN)
        {
            Gfx::startFrame();
            if(selected < (vids.size() - 1))
            {
                selected++;
                goto start;
            }
            Gfx::endFrame();
        }
    }
    Gfx::clear({ 255, 255, 255, 255 });
    if (appletMainLoop()){
        Player::playbackInit(vids[selected]);
    }
    Gfx::clear({255, 0, 0, 255});
    while(appletMainLoop() && Player::playbackLoop());
    Gfx::startFrame();
    Gfx::drawImage(0, 0, "romfs:/background.jpg");
    Gfx::drawText(75, 130, "Video playback finished.\n\nPress A to go back to video selection.\nPress Plus or Minus to exit PlayerNX.", { 255, 255, 255, 255 }, 20);
    Gfx::endFrame();
    while(appletMainLoop())
    {
        hidScanInput();
        u64 k = hidKeysDown(CONTROLLER_P1_AUTO);
        if(k & KEY_A) goto start;
        else if((k & KEY_PLUS) || (k & KEY_MINUS)) {
            break;
        }
    }
    cleanup();
    return 0;
}*/