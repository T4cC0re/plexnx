#include "clock.hpp"

bool Clock::initialized = false;

void Clock::initAuto() {
    if (!Clock::initialized) {
        printf("Initializing CPU OC...");
        if(hosversionBefore(8, 0, 0)) {
            pcvInitialize();
        } else {
            clkrstInitialize();
        }
        Clock::initialized = true;
        printf(" Done!\n");
    }
}

void Clock::reset() {
    Clock::runOC(1020);
    printf("Clock speed reset\n");
}

void Clock::exit() {
    if (!Clock::initialized) {
        return;
    }
    printf("Preparing CPU OC exit...");
    if(hosversionBefore(8, 0, 0)) {
        pcvExit();
    } else {
        clkrstExit();
    }
    printf(" Done!\n");
}

void Clock::runOC(u32 MHz) {
    Clock::initAuto();
    Result rc = 0;

    u64 hz = (u64)MHz * 1000 * 1000;

    printf("Trying to set CPU to %u MHz...", MHz);

    if (hosversionAtLeast(8, 0, 0)) {
        ClkrstSession session = {0};

        rc = clkrstOpenSession(&session, PcvModuleId_CpuBus, 3);
        if (rc != 0) {
            printf(" Failed to open session %u\n", rc);
            return;
        }

        rc = clkrstSetClockRate(&session, hz);
        if (rc != 0) {
            printf(" Failed to set clock %u\n", rc);
            return;
        }

        clkrstCloseSession(&session);
    } else {
        rc = pcvSetClockRate(PcvModule_CpuBus, hz);
        if (rc != 0) {
            printf(" Failed to set clock (<8.0.0) %u\n", rc);
            return;
        }
    }
    printf(" Success!\n");
}

u32 Clock::getClock() {
    Clock::initAuto();

    Result rc = 0;
    u32 hz = 0;

    if (hosversionAtLeast(8, 0, 0)) {
        ClkrstSession session = {0};

        rc = clkrstOpenSession(&session, PcvModuleId_CpuBus, 3);
        if (rc != 0) {
            return 0;
        }

        rc = clkrstGetClockRate(&session, &hz);
        if (rc != 0) {
            return 0;
        }

        clkrstCloseSession(&session);
    } else {
        rc = pcvGetClockRate(PcvModule_CpuBus, &hz);
        if (rc != 0) {
            return 0;
        }
    }

    return hz / 1000 / 1000;
}
