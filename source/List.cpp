#include "List.hpp"

template<typename T>
List<T>::List() {
    this->selected = -1;
    this->items = std::vector<T>();
    this->offsetX = 0;
    this->offsetY = 0;
    this->items.reserve(25);
}

template<typename T>
std::string List<T>::getItemDisplayName(int index) {
    return this->items.at(index).name;
}

template<>
std::string List<PlexSection>::getItemDisplayName(int index) {
    return this->items.at(index).title;
}

template<>
std::string List<PlexItem>::getItemDisplayName(int index) {
    return this->items.at(index).title;
}

/**
 * PlexItem uses specific implementation!
 * @tparam T
 */
template<typename T>
void List<T>::render() {

    Gfx::RGBA color = {128, 128, 128, 255};
    Gfx::RGBA selected = {255, 255, 255, 255};
    int renderOffsetY = this->offsetY;
    int renderOffsetX = this->offsetX;
    if (this->items.empty()) {
        Gfx::drawText(renderOffsetX, renderOffsetY, this->noItemsText, color, 20);
    }
    for (u32 i = 0; i < this->items.size(); i++) {
        if (this->getSelectedIndex() == (int) i) {
            Gfx::drawText(renderOffsetX, renderOffsetY, this->getItemDisplayName(i).c_str(), selected, 20);
        } else {
            Gfx::drawText(renderOffsetX, renderOffsetY, this->getItemDisplayName(i).c_str(), color, 20);
        }
        renderOffsetY += 25;
    }
}

template<>
void List<PlexItem>::render() {

    Gfx::RGBA color = {128, 128, 128, 255};
    Gfx::RGBA selected = {255, 255, 255, 255};
    int viewPortXmax = Gfx::FrameWidth - this->offsetX;
    int viewPortYmax = Gfx::FrameHeight - this->offsetY;

    int renderOffsetY = this->offsetY;
    int renderOffsetX = this->offsetX;
    if (this->items.empty()) {
        Gfx::drawText(renderOffsetX, renderOffsetY, this->noItemsText, color, 20);
    }
    int itemHeight = 250;
    int borderWidth = 5;
    int imageSize = 200; // DOES NOT DO SCALING! Layout only.
    u32 itemsPerPage = (Gfx::FrameHeight - this->offsetX) / itemHeight;
    u32 itemOffset = (u32)(this->getSelectedIndex() / itemsPerPage) * itemsPerPage;
    for (u32 i = itemOffset; i < std::min(itemOffset + itemsPerPage, (u32) this->items.size());
    i++) {
        PlexItem &item = this->items.at(i);

        if (renderOffsetY - this->offsetY + itemHeight > viewPortYmax) {
            renderOffsetY = this->offsetY;
        }

        Gfx::RGBA current;
        if (this->getSelectedIndex() == (int) i) {
            current = selected;
        } else {
            current = color;
        }

        Gfx::drawRectangle(renderOffsetX, renderOffsetY, imageSize + (2 * borderWidth), imageSize + (2 * borderWidth),
                           current);
        Plex::getInstance()->drawThumbnail(renderOffsetX + borderWidth, renderOffsetY + borderWidth, item);
        Gfx::drawText(
                renderOffsetX + imageSize + (2 * borderWidth) + 25,
                renderOffsetY + 120,
                item.title.c_str(),
                current,
                20
        );

        if (item.type.compare("show") == 0) {
            Gfx::drawText(
                    renderOffsetX + imageSize + (2 * borderWidth) + 25,
                    renderOffsetY + 145,
                    "TV Show",
                    current,
                    18
            );
        } else {
            Gfx::drawText(
                    renderOffsetX + imageSize + (2 * borderWidth) + 25,
                    renderOffsetY + 145,
                    std::to_string(item.duration / 1000 / 60).append(" Min.").c_str(),
                    current,
                    18
            );
        }
        renderOffsetY += itemHeight;
    }
}

template<typename T>
void List<T>::addItem(T item) {
    this->items.push_back(item);
    if (this->selected == -1) {
        this->selected = 0;
    }
    resizeVectorIfRequired(this->items);
}

//template<>
//void List<PlexItem>::addItem(PlexItem item) {
//    Plex::getInstance()->cacheThumbnail(item.server, item.id);
//    this->items.push_back(item);
//    if (this->selected == -1) {
//        this->selected = 0;
//    }
//}

template<typename T>
void List<T>::selectUp() {
    if (this->selected <= 0) {
        this->selected = ((int) this->items.size()) - 1;
    } else {
        this->selected--;
    }
}

template<typename T>
void List<T>::selectDown() {
    if (this->selected >= ((int) this->items.size()) - 1) {
        this->selected = 0;
    } else {
        this->selected++;
    }
}

template<typename T>
int List<T>::getSelectedIndex() {
    return this->selected;
}

template
class List<PlexServer>;

template
class List<PlexSection>;

template
class List<PlexItem>;