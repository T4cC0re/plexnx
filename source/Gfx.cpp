#include "Gfx.hpp"
#ifdef MEASURE_FRAME_TIME
#include <chrono>
std::chrono::steady_clock::time_point frameBegin;
#endif

std::mutex Gfx::mutex;

typedef union
{
    u32 abgr;
    struct
    {
        u8 r,g,b,a;
    };
} color_t;

typedef struct
{
    u8 width, height;
    int8_t posX, posY, advance;
    const u8* data;
} glyph_t;

typedef struct
{
    u8 magic[4];
    int version;
    u16 npages;
    u8 height;
    u8 baseline;
} ffnt_header_t;

typedef struct
{
    u32 size, offset;
} ffnt_pageentry_t;

typedef struct
{
    u32 pos[0x100];
    u8 widths[0x100];
    u8 heights[0x100];
    int8_t advances[0x100];
    int8_t posX[0x100];
    int8_t posY[0x100];
} ffnt_pagehdr_t;

typedef struct
{
    ffnt_pagehdr_t hdr;
    u8 data[];
} ffnt_page_t;

extern const ffnt_header_t InterUI14_nxfnt;
extern const ffnt_header_t InterUI18_nxfnt;
extern const ffnt_header_t InterUI20_nxfnt;
extern const ffnt_header_t InterUI24_nxfnt;

u8 bcolor(u32 src, u32 dst, u8 alpha)
{
    u8 one_minus_alpha = (u8)255 - alpha;
    return (dst * alpha + src * one_minus_alpha) / (u8)255;
}


Framebuffer Gfx::framebuffer;
u32* Gfx::currentFrame = nullptr;
u32 Gfx::currentStride;
NWindow *Gfx::nWindow;
u32 Gfx::FrameWidth;
u32 Gfx::FrameHeight;
bool initialized = false;
void pix(u32 x, u32 y, u8 r, u8 g, u8 b, u8 a, bool nolock = false) {
    if (x >= Gfx::FrameWidth || y >= Gfx::FrameHeight) return;
    u32 pos = y * Gfx::currentStride / sizeof(u32) + x;
    drawPos(pos, r, g, b, a);
}

const ffnt_page_t* FontGetPage(const ffnt_header_t* font, u32 page_id)
{
    if(page_id >= font->npages) return NULL;
    ffnt_pageentry_t* ent = &((ffnt_pageentry_t*)(font + 1))[page_id];
    if(ent->size == 0) return NULL;
    return (const ffnt_page_t*)((const u8*)font + ent->offset);
}

bool FontLoadGlyph(glyph_t* glyph, const ffnt_header_t* font, u32 codepoint)
{
    const ffnt_page_t* page = FontGetPage(font, codepoint >> 8);
    if(!page) return false;
    codepoint &= 0xFF;
    u32 off = page->hdr.pos[codepoint];
    if(off == ~(u32)0) return false;
    glyph->width = page->hdr.widths[codepoint];
    glyph->height = page->hdr.heights[codepoint];
    glyph->advance = page->hdr.advances[codepoint];
    glyph->posX = page->hdr.posX[codepoint];
    glyph->posY = page->hdr.posY[codepoint];
    glyph->data = &page->data[off];
    return true;
}

void DrawGlyph(u32 x, u32 y, color_t clr, const glyph_t* glyph)
{
    u32 i, j;
    const u8* data = glyph->data;
    x += glyph->posX;
    y += glyph->posY;
    std::lock_guard<std::mutex> lock(Gfx::mutex);
    for(j = 0; j < glyph->height; j ++)
    {
        for(i = 0; i < glyph->width; i ++)
        {
            clr.a = *data++;
            if(!clr.a) continue;
            pix(x + i, y + j, clr.r, clr.g, clr.b, clr.a);
        }
    }
}

u8 DecodeByte(const char** ptr)
{
    u8 c = (u8)**ptr;
    *ptr += 1;
    return c;
}

int8_t DecodeUTF8Cont(const char** ptr)
{
    int c = DecodeByte(ptr);
    return ((c & 0xC0) == 0x80) ? (c & 0x3F) : -1;
}

u32 DecodeUTF8(const char** ptr)
{
    u32 r;
    u8 c;
    int8_t c1, c2, c3;

    c = DecodeByte(ptr);
    if((c & 0x80) == 0) return c;
    if((c & 0xE0) == 0xC0)
    {
        c1 = DecodeUTF8Cont(ptr);
        if(c1 >= 0)
        {
            r = ((c & 0x1F) << 6) | c1;
            if(r >= 0x80) return r;
        }
    }
    else if((c & 0xF0) == 0xE0)
    {
        c1 = DecodeUTF8Cont(ptr);
        if(c1 >= 0)
        {
            c2 = DecodeUTF8Cont(ptr);
            if(c2 >= 0)
            {
                r = ((c & 0x0F) << 12) | (c1 << 6) | c2;
                if(r >= 0x800 && (r < 0xD800 || r >= 0xE000)) return r;
            }
        }
    }
    else if((c & 0xF8) == 0xF0)
    {
        c1 = DecodeUTF8Cont(ptr);
        if(c1 >= 0)
        {
            c2 = DecodeUTF8Cont(ptr);
            if(c2 >= 0)
            {
                c3 = DecodeUTF8Cont(ptr);
                if(c3 >= 0)
                {
                    r = ((c & 0x07) << 18) | (c1 << 12) | (c2 << 6) | c3;
                    if (r >= 0x10000 && r < 0x110000) return r;
                }
            }
        }
    }
    return 0xFFFD;
}

void DrawText_(const ffnt_header_t* font, u32 x, u32 y, color_t clr, const char* text, u32 max_width)
{
    y += font->baseline;
    u32 origX = x;
    while(*text)
    {
        if(max_width && x-origX >= max_width) break;

        glyph_t glyph;
        u32 codepoint = DecodeUTF8(&text);
        if(codepoint == '\n')
        {
            if(max_width) break;
            x = origX;
            y += font->height;
            continue;
        }

        if(!FontLoadGlyph(&glyph, font, codepoint))
        {
            if(!FontLoadGlyph(&glyph, font, '?')) continue;
        }
        DrawGlyph(x, y, clr, &glyph);
        x += glyph.advance;
    }
}

void Gfx::init()
{
    std::lock_guard<std::mutex> lock(Gfx::mutex);
    if (initialized) {
        return;
    }

    Gfx::nWindow = nwindowGetDefault();
    nwindowGetDimensions(Gfx::nWindow, &Gfx::FrameWidth, &Gfx::FrameHeight);
    /* result = */framebufferCreate(&Gfx::framebuffer, Gfx::nWindow, Gfx::FrameWidth, Gfx::FrameHeight,
                                    PIXEL_FORMAT_RGBA_8888, 2);
    framebufferMakeLinear(&Gfx::framebuffer);
    frameBegin = std::chrono::steady_clock::now();
    initialized = true;
}

void Gfx::drawPixel(u32 X, u32 Y, Gfx::RGBA Color) {
    std::lock_guard<std::mutex> lock(Gfx::mutex);
    pix(X, Y, Color.R, Color.G, Color.B, Color.A);
}

void Gfx::drawRectangle(u32 X, u32 Y, u32 Width, u32 Height, Gfx::RGBA Color) {
    for (u32 i = 0; i < Width; i++) for (u32 j = 0; j < Height; j++) Gfx::drawPixel(X + i, Y + j, Color);
}

void Gfx::drawText(u32 X, u32 Y, string Text, Gfx::RGBA Color, u32 Size) {
    color_t clr;
    clr.r = Color.R;
    clr.g = Color.G;
    clr.b = Color.B;
    clr.a = Color.A;
    const ffnt_header_t *font = &InterUI18_nxfnt;
    if (Size == 14) font = &InterUI14_nxfnt;
    else if (Size == 18) font = &InterUI18_nxfnt;
    else if (Size == 20) font = &InterUI20_nxfnt;
    else if (Size == 24) font = &InterUI24_nxfnt;
    DrawText_(font, X, Y, clr, Text.c_str(), 0);
}

void Gfx::drawImage(u32 X, u32 Y, string Path, uint8_t alpha, u32 maxX, u32 maxY, bool center) {
    std::lock_guard<std::mutex> lock(Gfx::mutex);
    string ext = Path.substr(Path.find_last_of(".") + 1);
    FILE *fjpg = fopen(Path.c_str(), "rb");
    if (fjpg) {
        fseek(fjpg, 0, SEEK_END);
        int size = (int) ftell(fjpg);
        u8 *buf = (u8 *) malloc(size);
        fseek(fjpg, 0, SEEK_SET);
        size = (int) fread(buf, 1, size, fjpg);
        fclose(fjpg);
        njInit();
        if (njDecode(buf, size) == NJ_OK) {
            u8 *raw = njGetImage();
            u32 w = njGetWidth();
            u32 h = njGetHeight();

            if (center) {
                X += (maxX - w) / 2;
                Y += (maxY - h) / 2;
            }

            std::lock_guard<std::mutex> lock(Gfx::mutex);
            for (u32 i = 0; i < w; i++) {
                if (i % 3 == 0 && w > 1280 && Gfx::FrameWidth <= 1280) {
                    continue;
                } else {
                    X++;
                    int y2 = Y;
                    for (u32 j = 0; j < h; j++) {
                        if (j % 3 == 0 && h > 720 && Gfx::FrameHeight <= 720) {
                            continue;
                        } else {
                            Y++;
                        }
                        u32 pos = ((j * w) + i) * 3;
                        pix(X, Y, raw[pos], raw[pos + 1], raw[pos + 2], alpha, true);
                    }
                    Y =y2;
                }
            }
        }
        njDone();
    }
    else {
        fclose(fjpg);
    }
}

void Gfx::drawImage(u32 X, u32 Y, string Path) {
    Gfx::drawImage(X, Y, Path, 255);
}

void Gfx::drawImage(u32 X, u32 Y, string Path, uint8_t alpha) {
    Gfx::drawImage(X, Y, Path, alpha, -1, -1, false);
}

void Gfx::startFrame()
{
    std::lock_guard<std::mutex> lock(Gfx::mutex);
    if (Gfx::currentFrame) {
        return;
    }
    Gfx::currentFrame = (u32 *) framebufferBegin(&Gfx::framebuffer, &Gfx::currentStride);
}

void Gfx::endFrame()
{
    std::lock_guard<std::mutex> lock(Gfx::mutex);
    if (!Gfx::currentFrame) {
        return;
    }
#ifdef MEASURE_FRAME_TIME
    std::chrono::steady_clock::time_point frameEnd = std::chrono::steady_clock::now();
    long diff = std::chrono::duration_cast<std::chrono::microseconds>(frameEnd - frameBegin).count();
    if (diff == 0) {diff = 1;} // prevent divide by 0
    gwrite(0, Gfx::FrameHeight - 40, 20, "[GFX] time since last frame: %lu µs, ~%lu FPS", diff, 1000000/diff);
    frameBegin = std::chrono::steady_clock::now();
#endif
    framebufferEnd(&Gfx::framebuffer);
    Gfx::currentFrame = nullptr;
}

void Gfx::clear(Gfx::RGBA Color)
{
    Gfx::startFrame();
    Gfx::drawRectangle(0, 0, Gfx::FrameWidth, Gfx::FrameHeight, Color);
    Gfx::endFrame();
}

void Gfx::exit()
{
    framebufferClose(&Gfx::framebuffer);
}