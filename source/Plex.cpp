#include "Plex.hpp"

#define xmlA(type, node, prefix, attribute) const type attribute = node -> prefix##ttribute(std::string( #attribute ).c_str())
#define xmlAtt(node, attribute) xmlA(char *, node, A, attribute)
#define xmlBool(node, attribute) xmlA(bool, node, BoolA, attribute)
#define xmlInt(node, attribute)  xmlA(int, node, IntA, attribute)
#define notGoto(name, next) if (! name ) {goto next ;};

Plex *instance = nullptr;

string nick = util::getDeviceNick();
string serialHash = util::getConsoleSerialHash();
string firmware = util::getFirmwareVersion();

struct curl_slist *getHeaders(const std::string& token, bool minimalHeaders) {
    struct curl_slist *headers = nullptr;
    if (!minimalHeaders) {
        headers = curl_slist_append(headers, "X-Plex-Platform: NX");
        headers = curl_slist_append(headers, (std::string("X-Plex-Platform-Version: ") + firmware).c_str());
        headers = curl_slist_append(headers, (std::string("X-Plex-Client-Identifier: ") + serialHash).c_str());
        headers = curl_slist_append(headers, "X-Plex-Product: "
        APP_TITLE_STR);
        headers = curl_slist_append(headers, "X-Plex-Version: "
        APP_VERSION_STR);
        headers = curl_slist_append(headers, "X-Plex-Device: Nintendo Switch");
        headers = curl_slist_append(headers, (std::string("X-Plex-Device-Name: ") + nick).c_str());
    }
    if (!token.empty()) {
        headers = curl_slist_append(headers, (std::string("X-Plex-Token: ") + token).c_str());
    }
    return headers;
};

Plex *Plex::getInstance() {
    if (instance == nullptr) {
        instance = new Plex();
    }

    return instance;
}

Plex::Plex() {
}

Plex::~Plex() {
}

size_t Plex::curlStringCallback(void *contents, size_t size, size_t nmemb, std::string *s) {
    size_t newLength = size * nmemb;
    try {
        s->append((char *) contents, newLength);
    }

    catch (std::bad_alloc &e) {
        //handle memory problem
        return 0;
    }
    return newLength;
}

std::string Plex::getAuthCodeByPin(int pinId) {
    char buff[100];
    snprintf(buff, sizeof(buff), "https://plex.tv/pins/%d.xml", pinId);
    std::string url = buff;
    tinyxml2::XMLDocument doc;
    std::string pinXML = this->simpleCurl(url, "GET");
    printf("%s\n", pinXML.c_str());
    doc.Parse(pinXML.c_str());

    //TODO: nullptr handling
    const char *token = doc.FirstChildElement("pin")->FirstChildElement("auth_token")->GetText();
    if (token == nullptr) {
        return "";
    }
    return std::string(token);
}

// https://www.plex.tv/link/
int Plex::requestPin() {
    tinyxml2::XMLDocument doc;
    std::string pinXML = this->simpleCurl("https://plex.tv/pins.xml", "POST");
    printf("%s\n", pinXML.c_str());
    doc.Parse(pinXML.c_str());

    //TODO: nullptr handling
    const char *pinId = doc.FirstChildElement("pin")->FirstChildElement("id")->GetText();
    const char *pinCode = doc.FirstChildElement("pin")->FirstChildElement("code")->GetText();

    int pinIdi = std::stoi(std::string(pinId), nullptr, 10);

    printf("pin IDi %d\n", pinIdi);

    gprintf("PLEX login PIN: %s\n\nPlease visit https://www.plex.tv/link/ to link your account.\nPress any button to cancel linkng.",
            pinCode);

    return pinIdi;
}

std::string Plex::tryReadAuthToken() {
    //TODO Load auth token from sdcard
    std::ifstream
    t("/config/"
    STRINGIFY(TARGET)
    "/token");
    std::stringstream buffer;
    buffer << t.rdbuf();
    return buffer.str();
}

void Plex::trySaveAuthToken(const std::string& token) {
    //TODO Save auth token to sdcard
    util::mkdirp("/config/"
    STRINGIFY(TARGET));

    std::ofstream
    out("/config/"
    STRINGIFY(TARGET)
    "/token");
    out << token;
    out.close();
}

void Plex::authRoutine() {
    int pinID = this->requestPin();
    std::string tmp;
    std::chrono::high_resolution_clock::time_point nextCheck =
            std::chrono::high_resolution_clock::now() + std::chrono::seconds(5);
    while (tmp.empty()) {
        ifAnyButton{
                gprintf("Link cancelled!\n");
                return;
        };
        if (nextCheck < std::chrono::high_resolution_clock::now()) {
            tmp = this->getAuthCodeByPin(pinID);
            nextCheck = std::chrono::high_resolution_clock::now() + std::chrono::seconds(5);
        }
    }
    this->trySaveAuthToken(tmp);
    gprintf("Link successful!\n");
}

bool pingURL(const std::string &in, const std::string &accessToken) {
    std::string url = in;
    std::string endpoint = std::string(url).append("/identity");
    debugf("endpoint: %s, token: %s\n", endpoint.c_str(), accessToken.c_str());

    std::string identity = Plex::getInstance()->simpleCurl(endpoint, "HEAD", accessToken, 500L);
    if (identity.empty()) {
        printf("no XML received. URL Offline.\n");
        return false;
    } else {
        return true;
    }
}

struct PlexServer parseDevice(tinyxml2::XMLElement *device) {
    struct PlexServer server;
    tinyxml2::XMLElement *connection;

    server.urls.reserve(2);

    xmlAtt(device, provides);
    if ((std::string(provides)).find("server") == std::string::npos) {
        printf("not a server Device node\n");
        goto next;
    }
    {
        xmlAtt(device, name);
        notGoto(name, next);
        xmlAtt(device, clientIdentifier);
        notGoto(clientIdentifier, next);
        xmlAtt(device, accessToken);
        notGoto(accessToken, next);
        xmlBool(device, httpsRequired);
        xmlBool(device, owned);

        server.httpsRequired = httpsRequired;
        server.owned = owned;
        server.name = std::string(name);
        server.clientIdentifier = std::string(clientIdentifier);
        server.accessToken = std::string(accessToken);
        connection = device->FirstChildElement("Connection");
        while (connection) {
            {
                xmlAtt(connection, uri);
                notGoto(uri, nextConn);

                auto url = std::string(uri);
                if (pingURL(url, accessToken)) {
                    server.urls.push_back(url);
                }
                resizeVectorIfRequired(server.urls);
            }

            nextConn:
            connection = connection->NextSiblingElement("Connection");
        }
    }

    //get next
    next:
    return server;
}

std::string Plex::getThumbnailLink(int item) {
    std::string prefix = "/photo/:/transcode?url=/library/metadata/";
    prefix.append(std::to_string(item));
    return prefix.append("/thumb&width=200&height=200");
}

std::string Plex::getPlaybackUrl(const struct PlexItem& item) {
//    tinyxml2::XMLDocument doc;
// TODO Make this only return the playback session, so other stuff such as ping and stop can be done.

    auto playbackSession = util::randomString(32);
    auto id2 = util::randomString(32);

    std::string path = item.key;
    util::replace(path, "/", "%2F");

    std::string startURL = item.server.urls.at(0);
    startURL.append("/video/:/transcode/universal/start.m3u8?hasMDE=1&path=");
    startURL.append(path);
    startURL.append("&mediaIndex=0");
    startURL.append("&partIndex=0&");
    startURL.append("protocol=hls");
    startURL.append("&fastSeek=1");
    startURL.append("&directPlay=0");
    startURL.append("&directStream=1");
    startURL.append("&subtitleSize=100");
    startURL.append("&audioBoost=100");
    startURL.append("&location=wan");
    startURL.append("&addDebugOverlay=0");
    startURL.append("&autoAdjustQuality=0");
    startURL.append("&directStreamAudio=1");
    startURL.append("&mediaBufferSize=102400");
    startURL.append("&session=");
    startURL.append(playbackSession);
    startURL.append("&subtitles=burn");
    startURL.append("&Accept-Language=en");
    startURL.append("&X-Plex-Session-Identifier=");
    startURL.append(id2);
    startURL.append("&X-Plex-Client-Profile-Extra=add-limitation%28scope%3DvideoCodec%26scopeName%3D%2A%26type%3DupperBound%26name%3Dvideo.width%26value%3D1280%26replace%3Dtrue%29%2Bappend-transcode-target-codec%28type%3DvideoProfile%26context%3Dstreaming%26audioCodec%3Daac%26protocol%3Ddash%29");
    startURL.append("&X-Plex-Platform=Wii%20U"); // Need to set "Wii U" here as there is no profile for NX/NintendoSwitch

    auto decisionUrl = startURL;
    util::replace(decisionUrl, "start.m3u8", "decision");

    std::string curl1 = "curl -iXGET ";
    curl1.append((std::string(" -H 'X-Plex-Client-Identifier: ") + serialHash + std::string("'")).c_str());
    curl1.append((std::string(" -H 'X-Plex-Token: ") + item.server.accessToken + std::string("'")).c_str());
    curl1.append(" '");
    curl1.append(decisionUrl);
    curl1.append("'");

    std::string decision = this->simpleCurl(decisionUrl, "GET", item.server.accessToken, 30000, true);
    printf("-----------decisionUrl-----------\n%s\n-------------------------\n", decision.c_str());
//    doc.Parse(xml.c_str());
    printf("%s\n", curl1.c_str());
    if (decision.empty()) {
        printf("no decision received. Unauthorized?\n");
        return "";
    }
    //TODO: decision is XML we should parse

    std::string curl = "curl -iXGET ";
    curl.append((std::string(" -H 'X-Plex-Client-Identifier: ") + serialHash + std::string("'")).c_str());
    curl.append((std::string(" -H 'X-Plex-Token: ") + item.server.accessToken + std::string("'")).c_str());
    curl.append(" '");
    curl.append(startURL);
    curl.append("'");

    std::string m3u8 = this->simpleCurl(startURL, "GET", item.server.accessToken, 30000, true);
    printf("-----------getPlaybackUrl-----------\n%s\n-------------------------\n", m3u8.c_str());
//    doc.Parse(xml.c_str());
    printf("%s\n", curl.c_str());
    if (m3u8.empty()) {
        printf("no m3u8 received. Unauthorized?\n");
        return "";
    }

    std::string indexm3u8 = item.server.urls.at(0);
    indexm3u8.append("/video/:/transcode/universal/session/");
    indexm3u8.append(playbackSession);
    indexm3u8.append("/base/index.m3u8");

    return indexm3u8;
}

/**
 * @brief Caches a thumbnail (resized to max 150x150) and returns the cacheKey. Downloads if required
 * @param server
 * @param item
 * @param thumbnailLink
 * @return
 */
std::string Plex::cacheThumbnail(struct PlexServer server, int item) {
    std::string cacheKey = server.clientIdentifier;
    cacheKey.append("-");
    cacheKey.append(std::to_string(item));
    cacheKey.append(".jpg");

    std::string filename("/cache/" TARGET_STR "/");
    filename.append(cacheKey);

    if (access(filename.c_str(), 0) == 0) {
        return cacheKey;
    }

    std::string link = server.urls.at(0);
    link.append(this->getThumbnailLink(item));

    if (this->downloadToCache(server.accessToken, link, cacheKey)) {
        return cacheKey;
    } else {
        printf("dl failed\n");
        return "";
    }

}

std::vector<struct PlexSection> Plex::connectToServer(const struct PlexServer& server) {
    std::vector<struct PlexSection> sections;

    std::string endpoint = server.urls.at(0);

    std::string curl = "curl -iXGET ";
    curl.append(" -H 'X-Plex-Platform: NX'");
    curl.append((std::string(" -H 'X-Plex-Platform-Version: ") + firmware + std::string("'")).c_str());
    curl.append((std::string(" -H 'X-Plex-Client-Identifier: ") + serialHash + std::string("'")).c_str());
    curl.append(" -H 'X-Plex-Product: " APP_TITLE_STR "'");
    curl.append(" -H 'X-Plex-Version: " APP_VERSION_STR "'");
    curl.append(" -H 'X-Plex-Device: Nintendo Switch'");
    curl.append((std::string(" -H 'X-Plex-Device-Name: ") + nick + std::string("'")).c_str());
    curl.append((std::string(" -H 'X-Plex-Token: ") + server.accessToken + std::string("'")).c_str());
    curl.append(" '");
    curl.append(endpoint);
    curl.append("'");

    printf("----------------------\n%s\n-------------------------\n", curl.c_str());

//    std::string key = this->cacheThumbnail(server, 598);
//    std::string filename("/cache/" TARGET_STR "/");
//    filename.append(key);
//    Gfx::drawImage(0, 0, filename);

    tinyxml2::XMLDocument doc;

    endpoint.append("/library/sections");
    std::string xml = this->simpleCurl(endpoint, "GET", server.accessToken, 10000L);
    doc.Parse(xml.c_str());

    auto mc = doc.FirstChildElement("MediaContainer");
    if (!mc) {
        return sections;
    }

    sections.reserve(10);

    auto directory = mc->FirstChildElement("Directory");
    while (directory) {
        {
            xmlAtt(directory, title);
            notGoto(title, next);
            xmlAtt(directory, type);
            notGoto(type, next);
            xmlBool(directory, allowSync);

            auto location = directory->FirstChildElement("Location");
            if (location) {
                xmlInt(location, id);

                PlexSection plexSection{
                        title,
                        type,
                        allowSync,
                        id,
                        server,
                };

                sections.push_back(plexSection);
                resizeVectorIfRequired(sections);
            }
        }

        next:
        directory = directory->NextSiblingElement("Directory");
    }

    return sections;

//    endpoint = server.urls.at(0);
//    endpoint.append("/library/sections/2/");
//    xml = this->simpleCurl(endpoint, "GET", server.accessToken, 10000L);
//    printf("----------------------\n%s\n-------------------------\n", xml.c_str());
}

std::vector<struct PlexItem> Plex::openKey(const std::string& key, const struct PlexServer& server) {
    tinyxml2::XMLDocument doc;
    std::vector<struct PlexItem> items;

    std::string url = server.urls.at(0);
    url.append(key);

    std::string xml = this->simpleCurl(url, "GET", server.accessToken);
    if (xml.empty()) {
        printf("no XML received. Unauthorized?\n");
        return items;
    }
    printf("-----------openKey-----------\n%s\n-------------------------\n", xml.c_str());
    doc.Parse(xml.c_str());


    auto mc = doc.FirstChildElement("MediaContainer");
    if (!mc) {
        printf("mc == nullptr\n");
        return items;
    }

    items.reserve(25);

    // This is for movies/misc videos
    auto elementName = "Video";
    auto node = mc->FirstChildElement(elementName);
    if (!node) {
        // This is for TV shows
        elementName = "Directory";
        node = mc->FirstChildElement(elementName);
    }
    while (node) {
        {
            xmlAtt(node, title);
            notGoto(title, next);
            xmlAtt(node, type);
            notGoto(type, next);
            xmlAtt(node, key);
            notGoto(key, next);
            xmlInt(node, duration);
            xmlInt(node, ratingKey);
            if (duration == -1 || ratingKey == -1) {
                goto next;
            }

            PlexItem plexItem{
                    title,
                    type,
                    ratingKey,
                    duration,
                    key,
                    server,
            };

            items.push_back(plexItem);
            resizeVectorIfRequired(items);

        }

        next:
        node = node->NextSiblingElement(elementName);
    }

    return items;
}

std::vector<struct PlexServer> Plex::getServers() {
    std::vector<struct PlexServer> servers;
    tinyxml2::XMLDocument doc;
    tinyxml2::XMLElement *devices;
    tinyxml2::XMLElement *device;
    std::string authToken = this->tryReadAuthToken();

    debugf("Starting request\n");
    std::string xml = this->simpleCurl("https://plex.tv/api/resources", "GET", authToken);
    if (xml.empty()) {
        printf("no XML received. Unauthorized?\n");
        return servers;
    }
    doc.Parse(xml.c_str());

    devices = doc.FirstChildElement("MediaContainer");
    if (!devices) {
        printf("devices == nullptr\n");
        return servers;
    }

    servers.reserve(5);

    device = devices->FirstChildElement("Device");
    while (device) {
        auto server = parseDevice(device);
        if (!server.urls.empty()) {
            servers.push_back(server);
            resizeVectorIfRequired(servers);
        }
        device = device->NextSiblingElement("Device");
    }

    debugf("All threads launched\n");
    return servers;
}

/**
 * Draw thumbnaul for item (downloads to cache if required)
 * @param x
 * @param y
 * @param item
 */
void Plex::drawThumbnail(int x, int y, const struct PlexItem& item) {
    //TODO: Find a way to draw centralized (in the center of 200x200);
    std::string key = this->cacheThumbnail(item.server, item.id);
    std::string filename("/cache/" TARGET_STR "/");
    filename.append(key);
    Gfx::drawImage(x, y, filename, 0xff, 200, 200, true);
}

std::string Plex::simpleCurl(const std::string& url, const std::string& method) {
    return this->simpleCurl(url, method, "", 0);
}

std::string Plex::simpleCurl(const std::string& url, const std::string& method, const std::string& accessToken) {
    return this->simpleCurl(url, method, accessToken, 0);
}

std::string Plex::simpleCurl(const std::string& url, const std::string& method, const std::string& accessToken, long timeoutMs) {
    return this->simpleCurl(url, method, accessToken, timeoutMs, false);
}

std::string Plex::simpleCurl(const std::string& url, const std::string& method, const std::string& accessToken, long timeoutMs, bool minimalHeaders) {
    CURLcode res;
    CURL *curl;
    std::string ret;

    auto headers = getHeaders(accessToken, minimalHeaders);

    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_USERAGENT, TARGET_STR
        "/"
        APP_VERSION_STR);
        curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 1024000L);
        if (method.compare(string("HEAD")) == 0) {
            curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
        }
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method.c_str());
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long) CURL_HTTP_VERSION_2TLS); //Try HTTP2
        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
//        curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 1L);
        curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 0L);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
#ifdef VERBOSE
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#else
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
#endif
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(curl, CURLOPT_TCP_NODELAY, 0); // Nagle is weird on NX
        curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L); // Non-OK response will fail the request
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 0L);
        if (timeoutMs) {
            curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT_MS, timeoutMs);
            curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, timeoutMs);
        }
        curl_easy_setopt(curl, CURLOPT_CAINFO, "romfs:/ca-bundle.pem");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Plex::curlStringCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ret);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        curl_slist_free_all(headers);
        if (res != CURLE_OK) {
            printf("\nfailed: %s\n", curl_easy_strerror(res));
        } else {
            printf("done!");

            if (method.compare(string("HEAD")) == 0) {
                return string("ok");
            }

            return ret;
        }
    } else {
        printf("curl initAuto failed");
    }

    return ret;
}

size_t write_callback(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    size_t written = fwrite(ptr, size, nmemb, stream);
    return written;
}

bool Plex::downloadToCache(const std::string& accessToken, const std::string& url, const std::string& cacheKey) {
    bool ret = false;
    CURLcode res;
    CURL *curl;
    FILE *fp;

    util::mkdirp("/cache/"
    TARGET_STR);
    std::string
    filename("/cache/"
    TARGET_STR
    "/");
    filename.append(cacheKey);

    auto headers = getHeaders(accessToken, false);

    curl = curl_easy_init();
    if (curl) {
        fp = fopen(filename.c_str(), "wb");
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_USERAGENT, TARGET_STR
        "/"
        APP_VERSION_STR);
        curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 1024000L);
        curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, (long) CURL_HTTP_VERSION_2TLS); //Try HTTP2
        curl_easy_setopt(curl, CURLOPT_TCP_KEEPALIVE, 1L);
//        curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 1L);
        curl_easy_setopt(curl, CURLOPT_FORBID_REUSE, 0L);
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
//        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_callback);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L); // following HTTP redirects
        curl_easy_setopt(curl, CURLOPT_TIMEOUT, 30L); // a max of timeout setopt for 20s
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback); // writes data into pointer
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp); // writes pointer into FILE *destination
        curl_easy_setopt(curl, CURLOPT_TCP_NODELAY, 0); // Nagle is weird on NX
        curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1L); // Non-OK response will fail the request
        curl_easy_setopt(curl, CURLOPT_CAINFO, "romfs:/ca-bundle.pem");

        res = curl_easy_perform(curl); // perform tasks curl_easy_setopt asked before
        curl_easy_cleanup(curl); // always cleanup
        curl_slist_free_all(headers); // Free header list before checking curl result
        fclose(fp); // closing FILE *fp
        if (res != CURLE_OK) {
            ret = false;
            unlink(filename.c_str());
        } else {
            ret = true;
        }
    } else {
        ret = false;
    }
    return ret;
}