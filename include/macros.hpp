#pragma once

#define drawPos(pos, r, g, b, a) Gfx::currentFrame[pos] = (a << 24) + (b << 16) + (g << 8) + r;
#define gprintf(...) {Gfx::startFrame();DRAW_BG();char buffer [1024];snprintf(buffer, 1024 ,__VA_ARGS__);Gfx::drawText(50, 150, buffer, { 255, 255, 255, 255 }, 20);Gfx::endFrame();}
#define gwrite(x, y, size, ...) {char buffer [1024];snprintf(buffer, 1024,__VA_ARGS__);Gfx::drawText(x,y, buffer, { 255, 255, 255, 255 }, size);}
#define haltf(...) gprintf(__VA_ARGS__);halt()
#define halt() while(true){hidScanInput(); u64 k = hidKeysDown(CONTROLLER_P1_AUTO); if((k & KEY_PLUS) || (k & KEY_MINUS)){cleanup();exit(0);}}

#define ifAnyButton hidScanInput(); u64 k = hidKeysDown(CONTROLLER_P1_AUTO); if(k)
#define DRAW_BG() {Gfx::drawImage(0, 0, "romfs:/background.jpg", 0xff);}

#define averr(n, ret) {char errbuf[512];av_make_error_string(errbuf, 512, AVERROR(ret)); haltf("AVERROR%d %s (%d)", n, errbuf, ret);}

#define resizeVectorIfRequired(vec) if (vec.size() == vec.capacity() && vec.size() > 0) { \
                                        vec.reserve(vec.capacity() * 2); \
                                    }

#ifdef VERBOSE
#define debugf(...) printf(__VA_ARGS__);
#else
#define debugf(...) {};
#endif

#define hexconcat(a,b) a ## x ## b
#define HEX(s) hexconcat(0, s)

#define STRINGIFY(s) xtr(s)
#define xtr(s) #s

#ifdef GPU_PALETTE_CONVERSION
#include "GPU.hpp"
#define cleanup() {	GPU::exit(); Gfx::exit(); romfsExit(); curl_global_cleanup(); Clock::exit(); consoleExit(NULL); socketExit();}
#else
#define cleanup() { Gfx::exit(); romfsExit(); curl_global_cleanup(); Clock::exit(); consoleExit(NULL); socketExit();}
#endif