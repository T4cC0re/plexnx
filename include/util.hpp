#pragma once

#include "Include.hpp"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include <sstream>

namespace util {
    std::string getDeviceNick();
    std::string getSha256(const std::string& data);
    std::string getConsoleSerialHash();
    std::string getFirmwareVersion();
    void mkdirp(std::string dir);
    bool deleteFolder(std::string folder);
    void replace(std::string &data, std::string toSearch, std::string replaceStr);
    std::string randomString(u32 length);
    std::string makeHex(const char* key, int length);
}