#pragma once

#include "Include.hpp"
#include "tinyxml2.h"
#include "Gfx.hpp"


struct PlexServer {
    std::string name;
    std::vector<std::string> urls;
    std::string clientIdentifier;
    bool owned;
    bool httpsRequired;
    std::string accessToken;
};

struct PlexSection {
    std::string title;
    std::string type;
    bool allowSync;
    int id;
    const PlexServer& server;
};


struct PlexItem {
    std::string title;
    std::string type;
    int id;
    int duration;
    std::string key;
    const PlexServer& server;
};

class Plex {
private:
    Plex();
    ~Plex();
public:
    static Plex* getInstance();
    std::vector<struct PlexSection> connectToServer(const struct PlexServer& server);
    std::vector<struct PlexItem> openKey(const std::string& key, const struct PlexServer& server);
    std::string getPlaybackUrl(const struct PlexItem& item);
    void drawThumbnail(int x, int y, const struct PlexItem& item);
    static size_t curlStringCallback(void *contents, size_t size, size_t nmemb, std::string *s);
    std::string getAuthCodeByPin(int pinId);
    std::string tryReadAuthToken();
    void trySaveAuthToken(const std::string& token);
    // https://www.plex.tv/link/
    int requestPin();
    void authRoutine();
    std::string simpleCurl(const std::string& url, const std::string& method);
    std::string simpleCurl(const std::string& url, const std::string& method, const std::string& accessToken);
    std::string simpleCurl(const std::string& url, const std::string& method, const std::string& accessToken, long timeoutMs);
    std::string simpleCurl(const std::string& url, const std::string& method, const std::string& accessToken, long timeoutMs, bool minimalHeaders);
    std::string cacheThumbnail(struct PlexServer server, int item);
    bool downloadToCache(const std::string& accessToken ,const std::string& url, const std::string& cacheKey);
    std::string getThumbnailLink(int item);
    std::vector<struct PlexServer> getServers();
};