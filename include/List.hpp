#pragma once
#include "Include.hpp"

template<typename T>
class List {
private:
    int selected;
public:
    std::string noItemsText;
    int offsetX;
    int offsetY;
    std::vector<T> items;
    List();
    void render();
    void addItem(T item);
    void selectUp();
    void selectDown();
    int getSelectedIndex();
    std::string getItemDisplayName(int index);
};
