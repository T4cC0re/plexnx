#pragma once
#include <chrono>

#define PACE_OK 0

class FramePacer {
private:
    int frameInterval;
    u64 frames;
    std::chrono::high_resolution_clock::time_point nextFrame;
    std::chrono::high_resolution_clock::time_point fpsReference;

public:

    FramePacer(const int frameInterval);

    ~FramePacer();

    int pace();

    double getFPS();
};
