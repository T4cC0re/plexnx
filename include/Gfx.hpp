
#pragma once
#include "Include.hpp"

namespace Gfx
{
    struct RGBA
    {
        u8 R;
        u8 G;
        u8 B;
        u8 A;
    };
    
    void init();
    void fadeImage(std::string Path, uint8_t startAlpha, int framesPerAlpha);
    void drawPixel(u32 X, u32 Y, RGBA Color);
    void drawRectangle(u32 X, u32 Y, u32 Width, u32 Height, RGBA Color);
    void drawText(u32 X, u32 Y, std::string Text, RGBA Color, u32 Size);
    void drawImage(u32 X, u32 Y, std::string Path);
    void drawImage(u32 X, u32 Y, std::string Path, uint8_t alpha);
    void drawImage(u32 X, u32 Y, std::string Path, uint8_t alpha, u32 maxX, u32 maxY, bool center);
    void startFrame();
    void endFrame();
    void clear(RGBA Color);
    void exit();

    extern std::mutex mutex;
    extern Framebuffer framebuffer;
    extern u32* currentFrame;
    extern u32 currentStride;
    extern NWindow *nWindow;
    extern u32 FrameWidth;
    extern u32 FrameHeight;
}