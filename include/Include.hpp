
#pragma once
#define __STDC_CONSTANT_MACROS

// macros
#include "macros.hpp"

#ifndef APP_VERSION
#error APP_VERSION is undefined
#endif
#define APP_VERSION_STR STRINGIFY(APP_VERSION)

#ifndef APP_TITLE
#error APP_TITLE is undefined
#endif
#define APP_TITLE_STR STRINGIFY(APP_TITLE)

#ifndef TARGET
#error TARGET is undefined
#endif
#define TARGET_STR STRINGIFY(TARGET)


extern "C"
{
    #include <libavutil/avutil.h>
    #include <libavutil/imgutils.h>
    #include <libavutil/opt.h>
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libswscale/swscale.h>
    #include <libswresample/swresample.h>
    #include "Gfx/nanojpeg.h"
}
#include <mutex>
#include <thread>
#include <memory>
#include <switch.h>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <iostream>
#include "clock.hpp"
#include "FramePacer.hpp"
#include "util.hpp"
#include "List.hpp"
#include "Plex.hpp"

#ifdef GPU_PALETTE_CONVERSION
#include "GPU.hpp"
#endif
// curl
#include <sys/select.h>
#include <curl/curl.h>
using namespace std;