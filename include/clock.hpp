
#pragma once
#include "Include.hpp"

namespace Clock {

    extern bool initialized;

    void initAuto();
    void reset();
    void runOC(u32 MHz);
    u32 getClock();
    void exit();
}